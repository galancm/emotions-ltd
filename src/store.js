import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const state = {
  cart: [],
  lastTransaction: {}
};

// get various details on store state
const getters = {
  cart(state) {
    return state.cart;
  },

  subTotal(state) {
    if (state.cart.length === 0) {
      return 0;
    } else {
      return state.cart
        .map((item) => item.price)
        .reduce((accumulator, price) => accumulator + price);
    }
  },
  lastTransaction(state) {
    return state.lastTransaction;
  }
};

// modify store state
const mutations = {
  addToCart(state, payload) {
    const inCart = state.cart.find((item) => {
      if (item.sku.id === payload.sku.id) {
        item.hours += payload.hours;
        return true;
      }
    });

    if (!inCart) {
      state.cart = [...state.cart, payload];
    }
  },
  removeFromCart(state, sku) {
    const skuIndex = state.cart.findIndex((item) => {
      return item.sku.id === sku.id;
    });
    state.cart.splice(skuIndex, 1);
  },
  emptyCart(state) {
    state.cart = [];
  },
  replaceLastTransaction(state, newTransaction) {
    state.lastTransaction = newTransaction;
  },
  clearLastTransaction(state) {
    state.lastTransaction = null;
  }
};

// asyncronous modifications to store state
const actions = {};

export default new Vuex.Store({
  state: state,
  getters: getters,
  mutations: mutations,
  actions: actions
});
