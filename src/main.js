import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

Vue.config.productionTip = false;

Vue.filter("dollars", function(value) {
  if (!value) return "";
  value = Math.round(value).toString();
  return value.slice(0, value.length - 2) + "." + value.slice(value.length - 2);
});

new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount("#app");
